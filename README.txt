# Bob the Wizard - LIFAPCD

Bob the Wizard est un jeu de role tour par tour dont le but est de tuer tous les ennemis.
Il se compile sous Linux, Windows WSL ou en ligne de commande.

Lien vers la forge: https://forge.univ-lyon1.fr/p2207310/montp
Fait par: Dylan Trontin 12105405, Thomas Bruniquel 12207310

## Le code et l'organisation des fichiers

Le code sépare les fonctions du noyau à celles de l'affichage gérer par la librairie SDL.

L'organistion des fichiers est la suivante:


- **src**    : Les classes du noyau du jeu avec la classe affichage gérant le code SDL
- **doc**    : Toute la documentation gérée par Doxygen
- **data**   : Les images du jeu pour l'affichage
- **bin**    : Les exécutables du jeu en mode texte ou graphique 
- **obj**    : Répertoire pour les fichiers objets lors de la compilation


## Pour compiler
La commande ```make``` permet de compiler et placer les exécutables dans le répertoire bin
Attention, la librairie SDL doit être installé sur votre machine.

### Pour installer SDL2 et Doxygen
Pour installer SDL2 sous Linux ou avec WSL/Windows
``` sudo apt install python3 valgrind doxygen libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev libsdl2-mixer-dev  ```

## Pour lancer les exécutables 
Pour la version graphique: ```bin/main```
Pour la version texte: ```bin/mainText```

##  Documentation du code
La documentation se trouve dans le fichier doc/html/index.html
ou peut se lancer par la commande ```firefox doc/html/index.html```

##  Comment marche le jeu
En lancant l'exécutable (graphique), vous retrouvez les différents éléments de gameplay du jeu. Vous avez à votre disposition:
	Un inventaire avec objets utilisable, notamment des potions.
	Différentes attaques dont les informations sont affichées en passant la souris dessus
	Des ennemis: la flèche représente l'ennemi qui va être attaqué. Il est possible de choisir qui attaquer en cliquant sur l'ennemi désiré.
Tour de role: A chaque tour, vous avez le choix entre plusieurs attaques/sorts. Utiliser un objet de l'inventaire ne passe pas votre tour, seul
attaquer un ennemi le fait. Ensuite, c'est au tour des ennemis de vous attaquer. A la fin d'un niveau, c'est à dire quand tous les ennemis
sont morts, vous avez le choix entre recommencer la partie ou passer au niveau suivant. La partie se finit lorsque vous avez réussi tous les 
niveaux, ou lorsque vous êtes mort.
