#ifndef INVENTORY_H
#define INVENTORY_H
#include "objet.h"
#include <vector>

/**
 * @class Inventory
 * @brief Classe représentant l'inventaire d'un Personnage.
 */
class Inventory {
    private:
        // Inventaire max : 10 objets
        std::vector<Objet> objs;
        int money;

    public:
        Inventory();
        Inventory(const std::vector<Objet>& newObjs, int newMoney);
        ~Inventory();
        friend std::ostream& operator<<(std::ostream& os, const Inventory& inv);

        std::vector<Objet> getAllObj() const;
        Objet getObjIndex(int index) const;
        int getMoney() const;
        unsigned int getNbObj() const;
        
        void addObject (const Objet& newObj);
        void removeObject (const unsigned int index);
        void addMoney (int ammount);
};
#endif