#include <assert.h>
#include <string>
#include <iostream>
#include "image.h"
using namespace std;

Image::Image() { surface=nullptr; texture=nullptr; SDL_Rect Rec = {0, 0, 0, 0}; position= Rec;}
Image::Image(SDL_Renderer * renderer, const char* filename,int posx, int posy, int width, int height) {
    int w, h;
    SDL_GetRendererOutputSize(renderer, &w, &h);
    assert(posx+width<=w);
    assert(posy+height<=h);

    surface=IMG_Load(filename);
    if (surface==0) {
        cout<<"Le chemin "<<filename<<" vers le fichier est incorrect";
        exit(1);
    } else {
        cout<< "Succes du chargement de "<<filename<<endl;
    }
    texture=SDL_CreateTextureFromSurface(renderer,surface);
    position= {posx,posy,width,height};
}


Image::~Image() {
    if (texture==nullptr)
        SDL_DestroyTexture(texture);
    if (surface==nullptr)
        SDL_FreeSurface(surface);
    surface = nullptr;
    texture = nullptr;
}

/** @brief Initialise une image
 *  @param renderer Renderer du sdl
 *  @param filenamme Nom du fichier contenant l'image
 *  @param posx Position de l'image en x
 *  @param posy Position de l'image en y
 *  @param width Largueur de l'image
 *  @param height Hauteur de l'image
 */
void Image::initImage(SDL_Renderer * renderer, const char* filename,int posx, int posy, int width, int height) {
    int w, h;
    SDL_GetRendererOutputSize(renderer, &w, &h);
    assert(posx+width<=w);
    assert(posy+height<=h);

    surface=IMG_Load(filename);
    if (surface==0) {
        cout<<"Le chemin "<<filename<<" vers le fichier est incorrect";
        exit(1);
    } else {
        cout<< "Succes du chargement de "<<filename<<endl;
    }
    texture=SDL_CreateTextureFromSurface(renderer,surface);
    position= {posx,posy,width,height};
}
/** @brief Modifie la surface
 *  @param newSurface Nouvelle surface de l'image
 */
void Image::setSurface(SDL_Surface * newSurface) {surface=newSurface;}