#ifndef CARACTERISTIQUE_H
#define CARACTERISTIQUE_H
#include <iostream>
/**
 * @class Caracteristique
 * @brief Classe representant les caractéristiques des Personnage du jeu. Cette classe contient des attributs comme force, vie, vitesse, mana, armure.
 */
class Caracteristique {
private:

    int strength;
    int health,maxHealth;
    int mana,maxMana;
    int speed;
    int armor;


public:
    Caracteristique();
    Caracteristique(int newStrength, int newHealth, int newMana, int newSpeed, int newArmor);
    friend std::ostream& operator<<(std::ostream& os, const Caracteristique& carac);

    int getStrength() const;
    int getHealth() const;
    int getMaxHealth() const;
    int getMana() const;
    int getMaxMana() const;
    int getSpeed() const;
    int getArmor() const;
    void setStrength(unsigned int newStrength);
    void setSpeed(unsigned int newSpeed);
    void setArmor(unsigned int newArmor);
    int addHealth(int ammount);
    void addMana(int ammount); 
    void addStrength(int amount);
};

#endif