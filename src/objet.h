#ifndef OBJET_H
#define OBJET_H
#include "caracteristique.h"
#include <string.h>
enum type_obj {DEFAULT,HEALTH_POT,MANA_POT,STRENGTH_POT};

/**
 * @class Objet
 * @brief Classe contenant les informations d'un objet de l'Inventory
 */
class Objet {
    private:
        type_obj objType;
        Caracteristique carac;
        int duration;

    public:
        Objet();
        Objet(type_obj newObjType,Caracteristique carac,int newDuration);
        friend std::ostream& operator<<(std::ostream& os, const Objet& obj);
        Caracteristique getCarac() const;
        int getDuration() const;
        type_obj getTypeObj() const;
        std::string getObjName() const;
        
};
#endif