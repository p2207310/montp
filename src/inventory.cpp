#include <iostream>
#include <assert.h>
#include "inventory.h"
#include <map>
#include <algorithm>
using namespace std;


Inventory::Inventory() {
    money=0;
}

Inventory::Inventory(const vector<Objet>& newObjs, int newMoney) {
    assert (newObjs.size()<=8);
    money=newMoney;
    objs=newObjs;
    sort(objs.begin(),objs.end(),[](Objet a,Objet b)-> bool
   {return a.getTypeObj()<b.getTypeObj();});
}

Inventory::~Inventory() { }

ostream& operator<<(ostream& os, const Inventory& inv)
{
    os<<"Nombre d'objets: "<<inv.getNbObj()<<" | Argent: "<<inv.getMoney()<<"$"<<endl;
    os<<"Liste des objets:"<<endl;
    for (unsigned int i=0;i<inv.getNbObj();i++)
        os<<"Objet "<<i<<": "<<inv.getObjIndex(i);
    return os;
}
/** @brief Retourne le tableau d'objet de l'inventaire */
vector<Objet> Inventory::getAllObj() const{ return objs; }

/** @brief Récupère un objet
 *  @param index Index de l'objet
 */
Objet Inventory::getObjIndex(int index) const{
    assert (index<8);
    return objs[index];
}
/** @brief Récupère l'argent */
int Inventory::getMoney() const {return money;}

/** @brief Récupère le nombre d'objet présent */
unsigned int Inventory::getNbObj() const {return objs.size();}

/** @brief Ajoute un objet
 *  @param newObj Objet à ajouter
 */
void Inventory::addObject (const Objet& newObj) {
    assert(objs.size()<8);
    objs.push_back(newObj);
}

/** @brief Enlève un objet
 *  @param index Index de l'objet à enlever
 */
void Inventory::removeObject (const unsigned int index) {
    assert (index<getNbObj());
    objs.erase(objs.begin()+index);
}

/** @brief Ajoute de l'argent
 *  @param ammount Montant à ajouter
 */
void Inventory::addMoney (int ammount) {
    if(money+ammount<0)
        money=0;
    else
        money+=ammount;
}