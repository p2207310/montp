#ifndef SORT_H
#define SORT_H

/**
 * @class Sort
 * @brief Classe définissant les informations d'un sort d'un Personnage
 */
class Sort {
    private:
        int manaCost;
        float strengthRatio;
        int dmgFix;

    public:
        friend std::ostream& operator<<(std::ostream& os, const Sort& sort);
        Sort();
        Sort(int newManaCost, float newStrengthRatio, int newDmgFix);
        int getManaCost() const;
        float getStrengthRatio() const;
        int getDmgFix() const;
};
#endif