#ifndef IMAGE_H
#define IMAGE_H
#include <SDL.h>
#include <SDL_image.h>
/**
 * @class Image
 * @brief Classe regroupant la surface, texture et position pour un affichage SDL
 */
class Image {
    private:

    public:
        SDL_Surface * surface;
        SDL_Texture * texture;
        SDL_Rect position;
        Image();
        ~Image();
        Image(SDL_Renderer * renderer, const char* filename,int posx, int posy, int width, int height);
        void initImage(SDL_Renderer * renderer, const char* filename,int posx, int posy, int width, int height);
        void setSurface(SDL_Surface * newSurface);
};
#endif
