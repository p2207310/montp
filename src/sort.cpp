#include <iostream>
#include "sort.h"
using namespace std;
#include <assert.h>

ostream& operator<<(ostream& os, const Sort& sort)
{
    os << "Cout en mana: "<<sort.getManaCost() << " | Ratio force: "<<sort.getStrengthRatio()<<" | Degats Fix "<<sort.getDmgFix()<<endl;
    return os;
}
Sort::Sort(){
    manaCost = 0;
    strengthRatio = 0;
    dmgFix = 0;
}
Sort::Sort(int newManaCost, float newStrengthRatio, int newDmgFix){
    assert(newManaCost>=0);
    assert(newStrengthRatio>=0);
    assert(newDmgFix>=0);
    manaCost = newManaCost;
    strengthRatio = newStrengthRatio; 
    dmgFix = newDmgFix;
}

/** @brief Récupère le cout en mana */
int Sort::getManaCost() const { return manaCost; }

/** @brief Récupère le ratio de force */
float Sort::getStrengthRatio() const { return strengthRatio; }

/** @brief Récupère les dégats fixes */
int Sort::getDmgFix() const { return dmgFix; }
