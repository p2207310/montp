#include <assert.h>
#include "jeu.h"
#include <map>
#include <algorithm>
using namespace std;

struct Niveau niveau;

Jeu::Jeu(const std::vector<Niveau>& newNiveaux,const int newNivActuel, const Personnage& newPlayer){
    niveaux=newNiveaux;
    nivActuel=newNivActuel;
    player=newPlayer;
}

Jeu::Jeu(){
    nivActuel=-1;
    endGame=0;
}

ostream& operator<<(ostream& os, const Jeu& jeu)
{
    os<<"------------------------------------------------------------------------"<<endl;
    os<<"Liste des Personnages dans l'ordre de priorite:"<<endl;
    for (unsigned int i=0;i<jeu.prio.size();i++){
        os<<jeu.prio[i];
        os<<"------------------------------------------------------------------------"<<endl;
    }
    os<<endl;
    return os;
}

void Jeu::initJeu() {
    Sort spell_1(5,1.2,3);
    Sort spell_2(11,1.5,5);
    std::vector<Sort> spells;
    spells.push_back(spell_1);
    spells.push_back(spell_2);
    Stuff stuff_1;
    Stuff stuff_2(10,15,20);
   
    Caracteristique carac_1(0,20,0,0,0);
    Caracteristique carac_2(20,50,20,10,2);
    Caracteristique carac_3(0,0,10,0,0);
    Caracteristique carac_4(5,50,20,10,2);
    Caracteristique carac_5(15,0,0,0,0);
    Caracteristique carac_6(8,60,20,10,6);
    Caracteristique carac_7(12,35,20,10,6);

    Objet obj_1(HEALTH_POT, carac_1, 0);
    Objet obj_2(HEALTH_POT, carac_1, 0);
    Objet obj_3(MANA_POT, carac_3,0);
    Objet obj_4(STRENGTH_POT, carac_5,3);
    vector<Objet> listeObj (5);
    listeObj[0] = obj_1;
    listeObj[1] = obj_2;
    listeObj[2] = obj_3;
    listeObj[3] = obj_4;
    listeObj[4] = obj_3;
    Inventory inventory_1(listeObj, 40);

    Personnage perso_2(0,1,"Dylon Bronzos",carac_2,inventory_1,stuff_1,spells);
    Personnage perso_3(1,1,"ADC",carac_4,inventory_1,stuff_1,spells);
    Personnage perso_4(1,1,"Support",carac_7,inventory_1,stuff_1,spells);
    
    Niveau niv1;
    niv1.nbEnnemisMorts = 0;
    niv1.persoNiv.push_back(perso_3);
    niv1.persoNiv.push_back(perso_4);

    Personnage perso_Niv2(1,2,"Corbac", carac_4, inventory_1, stuff_1, spells);
    Personnage perso_Niv2_2(1,2,"Gromp", carac_6, inventory_1, stuff_1, spells);

    Niveau niv2;
    niv2.nbEnnemisMorts = 0;
    niv2.persoNiv.push_back(perso_Niv2);
    niv2.persoNiv.push_back(perso_Niv2_2);

    niveaux.push_back(niv1);
    niveaux.push_back(niv2);
    nivActuel=0;
    player = perso_2;      
}
/** @brief increment le niveau actuel */
void Jeu::incrementNivActuel() {nivActuel +=1;}


/** @brief restart l'etat du jeu */
void Jeu::restart(){
    niveaux.clear();
    initJeu();
}


/** @brief retourne le nb de niveaux */
int Jeu::nbNiv() {return niveaux.size();}

/** @brief Retourne l'entier correspondant au niveau actuel */
int Jeu::getNivActuel() const {return nivActuel;}

/** @brief Retourne le nombre d'ennemi présent sur le niveau actuel */
unsigned int Jeu::getEnemyNumber() const {return niveaux[getNivActuel()].persoNiv.size();}

/** @brief Initialise un tableau d'identifiant correspond à l'ordre de jeu de chaque entités présentes sur le niveau. La priorité est calculé par rapport à l'attribut de vitesse */
void Jeu::initPriorite(){
   vector<Personnage> prioTemp;
   if(prio.size()!=0){
        prio.clear();
    }
   prioTemp.push_back(player);
   for(unsigned int i=0;i<getEnemyNumber();i++){
    prioTemp.push_back((niveaux[getNivActuel()].persoNiv[i]) );
   }
    sort(prioTemp.begin(),prioTemp.end(),[](Personnage a,Personnage b)-> bool
   {return a.getCaracPerso().getSpeed()>b.getCaracPerso().getSpeed();});
    for (unsigned int j = 0;j<prioTemp.size();j++){
        prio.push_back(prioTemp[j].getId());
    }
}
/** @brief Récupère le nombre d'ennemi mort sur le niveau actuel */
unsigned int Jeu::getNbEnnemiMort() {return niveaux[getNivActuel()].nbEnnemisMorts;}

/** @brief Retourne le niveau actuel */
vector<Niveau>& Jeu::getNiveaux(){ return niveaux;}

/** @brief Assigne un identifiant à toute les entités du niveau actuel */
void Jeu::assignId() {
    player.setId(0);
    for (unsigned int nbNiv=0;nbNiv<niveaux.size();nbNiv++) {
        int currId=10*(nbNiv+1);
        for (unsigned int i=0;i<niveaux[nbNiv].persoNiv.size();i++) {
            niveaux[nbNiv].persoNiv[i].setId(currId);
            currId++;
        }
    }
}

/** @brief Retourne un Personnage en fonction de son identifiant */
Personnage& Jeu::getPlayerById(unsigned int id) { //a débug assert
    if (id==0) return player;
    else {
        int nbNiv=id/10;
        int indexPlayer=id%10;
        return niveaux[nbNiv-1].persoNiv[indexPlayer];
    }
}

/** @brief Retourne un Personnage en fonction de son identifiant */
Personnage& Jeu::getPlayer() {return player;}

/** @brief Défini les comportements en fonctions des commandes tapés (jeu en mode texte) */
void Jeu::commandsHandler(string command,bool& endRound,bool& endOfAction,const vector<unsigned int>& prio){
    //Switch case non valable sur les string donc on enchaine des if
    if (command=="!help") {
        cout<<"Voici la liste des commandes disponibles:"<<endl<<"---"<<flush;
        cout<<"!me : Affiche vos informations."<<endl<<"---"<<flush;;
        cout<<"!perso : Affiche la liste des ennemis presents sur le niveau."<<endl<<"---"<<flush;
        cout<<"!attack : Attaque un ennemi."<<endl<<"---"<<flush;
        cout<<"!use : Utilise un objet dans l'inventaire."<<endl<<"---"<<flush;
        cout<<"!q : Quitte le jeu."<<endl;

    } else if (command=="!attack") {
        bool valid=false;
        while (!valid) {
            cout<<"---Choisissez la cible: (1 pour le premier ennemi, 2 pour le second...)"<<endl<<"---"<<flush;
            unsigned int ennemyNumChoice;
            cin>>ennemyNumChoice;
            //Si choix de l'ennemi valide
            if (ennemyNumChoice<prio.size()) {
                cout<<"------Choisissez quelle attaque utiliser (0 pour l'auto-attaque, 1 pour le premier sort, 2 pour le deuxieme...)"<<endl<<"------"<<flush;
                unsigned int spellChoice;
                cin>>spellChoice;
                //Si choix de sort valide
                if (spellChoice<=player.getNbSpell()) {
                    retourAttack ra;
                    //Si auto attaque
                    if (spellChoice==0){
                        ra=player.attack(getPlayerById(prio[ennemyNumChoice]));
                    } else { //Sinon si sort
                        ra=player.castSpell(spellChoice-1,getPlayerById(prio[ennemyNumChoice]));
                        cout<<"votre mana: "<<getPlayerById(0).getCaracPerso().getMana()<<" spell: "<<getPlayerById(0).sortsPerso[spellChoice-1].getManaCost()<<endl;
                    }
                    if (ra.killed){
                        niveaux[getNivActuel()].nbEnnemisMorts++;
                    }
                    valid=true;
                    endRound=true;
                    if (ra.applied){ 
                        endOfAction = true;
                    }
                    else{
                        if (getPlayerById(prio[ennemyNumChoice]).isAlive())
                            cout << "Pas assez de mana" << endl;
                        else
                            cout<< "L'ennemi est deja mort!"<<endl;
                    }
                    
                } else {
                    cout<<"Veuillez choisir un numero de sort valide.";
                }
                
            } else {
                cout<<"L'ennemi choisi n'est pas valide"<<endl;
            }
        }
    } else if (command=="!use"){ //Utilise un objet dans l'inventaire
        cout<<"---Quel objet voulez vous utiliser (0 pour le premier item, 1 pour le second ...)"<<endl<<"---"<<flush;
        unsigned int objIndex;
        cin>>objIndex;
        getPlayerById(0).useObject(objIndex);
    } else if (command=="!perso"){ //Affiche les informations des ennemis
        cout<<"Liste des perso :"<<endl<<"---"<<flush;
        unsigned int nbPerso=prio.size();
        cout<<prio.size()<<endl;

        //On s'arrete avant pour le style et ne pas afficher les "---" après
        for (unsigned int i=0;i<nbPerso;i++){
            cout<<"prio: "<<prio[0]<<endl;
            cout<< getPlayerById(prio[i]).getNamePerso()<< " : "<<getPlayerById(prio[i]).getCaracPerso()<<endl<<"---"<<flush;;
        }
    } else if (command=="!me"){ //Affiche les informations du joueur
        cout<<"Voici vos informations:"<<endl;
        cout<<"---"<<getPlayerById(0).getNamePerso()<<": "<<getPlayerById(0).getCaracPerso()<<endl<<"---"<<flush;
        cout<<"Attaque disponibles: "<<endl<<"---Auto attaque"<<endl;
        for (unsigned int i=0;i<getPlayerById(0).getNbSpell();i++) {
            cout<<"---Sort "<<i+1<<": "<<getPlayerById(0).getSortPerso(i);
        }
        cout<<endl;
        cout<<"---"<<getPlayerById(0).getInvPerso()<<endl;
    } else if (command=="!q"){ //Quitte le script (un peu brutalement)
        exit(0);
    } else { //Commande incorrecte
        cout<<"Cette commande n'est pas disponible."<<endl;
    }
}

/** @brief Boucle de jeu en mode texte */
void Jeu::play(bool nextLevel=false){
    bool endRound; //Fin du tour
    endGame=false;
    string command;
    if(nextLevel) {
        int nextNiv=nivActuel++;
        initJeu();
        nivActuel=nextNiv;
    } else {
        initJeu();
    }
    assignId();
    initPriorite();//niv actuel
    unsigned int nbEnnemi = getEnemyNumber();
    std::string namePlayer = player.getNamePerso();
    unsigned nbPerso =prio.size();
    while (!endGame) {
        endRound=false;
        if(!endRound){
            for (unsigned i = 0;i<nbPerso;i++){
                if (getPlayerById(0).isAlive()){
                    bool endOfAction = false;
                    std::string persoName = getPlayerById(0).getNamePerso();

                    if (prio[i] == 0){ 
                        command="";
                        cout<<endl<<"Veuillez entrer votre commande (!help pour voir la liste des commandes):"<<endl;
                        /*cout << !player.isAlive() << endl;
                        cout << (nbEnnemi == niveaux[getNivActuel()].nbEnnemisMorts) << endl;
                        cout << niveaux[getNivActuel()].nbEnnemisMorts << endl;
                        cout << nbEnnemi << endl;*/
                        while(!endOfAction){
                            cin>>command;
                            commandsHandler(command,endRound,endOfAction,prio);
                            if (!endOfAction){
                                cout<<endl<<"Veuillez entrer votre commande (!help pour voir la liste des commandes):"<<endl;
                            }
                            }
                    }
                    else{
                        retourAttack retourAttackEnnemi;
                        if(getPlayerById(prio[i]).isAlive())
                            retourAttackEnnemi = getPlayerById(prio[i]).playAgainst(getPlayerById(0)); //pour l'instant ils ne font que auto attack 
                        endOfAction = true;
                    }
                }
            }
            
        }
        endGame = ((!getPlayerById(0).isAlive()) || (nbEnnemi == getNbEnnemiMort()));
    }
    cout<<"------------------------------------------------------------------------"<<endl;
    if(getPlayerById(0).isAlive() && getNivActuel() < nbNiv()-1){
        cout<<"Felicitations,vous avez battu le niveau "<<1+getNivActuel()<<"!"<<endl;
        incrementNivActuel();
        cout<<"Passage au niveau suivant..."<<endl;
        cout<<"------------------------------------------------------------------------"<<endl;
        play(true);
    }
    else if(getPlayerById(0).isAlive())
        cout<<"Vous avez battu le jeu, félicitations !"<<endl;
    else
        cout<<"Vous etes mort au niveau "<<1+getNivActuel()<<"..."<<endl;
}
