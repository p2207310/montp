#include <iostream>
#include "objet.h"
#include <assert.h>
#include <string.h>
using namespace std;


ostream& operator<<(ostream& os, const Objet& obj)
{
    os << "Nom: "<<obj.getObjName()<<" | Caracteristique de l'objet: "<<obj.getCarac() << " | Ratio force: "<<obj.getDuration()<<endl;
    return os;
}
Objet::Objet(){
    objType = DEFAULT;
    duration = 1;
}
Objet::Objet(type_obj newObjType, Caracteristique newCarac, int newDuration){
    assert(newDuration>=0);
    objType = newObjType;
    carac = newCarac;
    duration = newDuration;
}

/** @brief Retourne la durée */
int Objet::getDuration() const { return duration; }

/** @brief Récupère les caractéristiques */
Caracteristique Objet::getCarac() const { return carac; }

/** @brief Récupère le type d'objet */
type_obj Objet::getTypeObj() const {return objType;}

/** @brief Récupère le nom  */
string Objet::getObjName() const { 
    if(getTypeObj()==DEFAULT){
        return "objet defaut";
    }
    else if(getTypeObj()==HEALTH_POT){
        return "potion de soin";
    }
    else if(getTypeObj()==MANA_POT){
        return "potion de mana";
    }
    else if(getTypeObj()==STRENGTH_POT){
        return "potion de force";
    }
    return "objet defaut";
}

