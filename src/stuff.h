#include <iostream>
#ifndef STUFF_H
#define STUFF_H

/**
 * @class Stuff
 * @brief Classe définissant l'équipement d'un Personnage
 */
class Stuff {
    private:
    
        int helmetArmor;
        int chestArmor;
        int weaponDmg;

    public:
    /** @brief Constructeurs et surchage opérateurs */
        Stuff();
        Stuff(int newHelmetArmor, int newChestArmor, int newWeaponDmg);
        friend std::ostream& operator<<(std::ostream& os, const Stuff& stuff);
        /** @brief Getters et setters */
        int getHelmetArmor() const;
        int getChestArmor() const;
        int getWeaponDmg() const;
        void setHelmetArmor(int newArmor);
        void setChestArmor(int newArmor);
        void setWeaponDmg(int newDmg);
};
#endif