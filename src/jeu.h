#ifndef JEU_H
#define JEU_H
#include "personnage.h"
#include <vector>
/**
 * @struct Niveau
 * @brief Structure: les niveaux sont composés d'un numéro(index dans le tableau) et de tous les Personnage rattachés
 */
struct Niveau{
    std::vector<Personnage> persoNiv;
    unsigned int nbEnnemisMorts;
};

/**
 * @class Jeu
 * @brief Classe définissant un jeu: il y a le joueur principal, et un tableau de Niveau
 */
class Jeu {
    private:
        std::vector<Niveau> niveaux;
        int nivActuel;
        Personnage player;
    public:
        std::vector<unsigned int> prio;
        bool endGame;
        Jeu();
        Jeu(const std::vector<Niveau>& newNiveaux,const int newNivActuel, const Personnage& newPlayer);
        friend std::ostream& operator<<(std::ostream& os, const Jeu& jeu);
        int getNivActuel() const;
        unsigned int getEnemyNumber() const;
        Niveau getNivIndex(int Index) const;
        Personnage &getPlayerById(unsigned int id);
        Personnage &getPlayer();
        unsigned int getNbEnnemiMort();
        std::vector<Niveau> &getNiveaux();
        void initJeu();
        void initPriorite();
        void incrementNivActuel();
        void assignId();
        int nbNiv();
        void restart();
        void commandsHandler(std::string command,bool& endRound, bool& endOfAction,const std::vector<unsigned int>& prio);
        void play(bool nextLevel);

};
#endif