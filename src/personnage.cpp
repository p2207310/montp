#include <iostream>
#include "personnage.h"
using namespace std;
#include <assert.h>


ostream& operator<<(ostream& os, Personnage& perso)
{
    os << "Nom perso: "<<perso.getNamePerso() << " | Niveau: "<<perso.getNivPerso()<<" | Id: " <<perso.getId()<<endl <<"Caracteristiques: "<<perso.getCaracPerso();
    os << endl << "Inventaire: "<< perso.getInvPerso() << endl;
    os << "Equipement: " <<perso.getStuffPerso()<<endl;
    os << "Sorts: " << endl;
    for (unsigned int i=0;i<perso.getNbSpell();i++){
        os<<perso.getSortPerso(i);
    }
    os << endl;
    return os;
}
Personnage::Personnage(){
    namePerso = "NPC";
    nivPerso = 1;
    Sort spell;
    sortsPerso.push_back(spell);
}
Personnage::Personnage(unsigned int newId,int newNiv,const string newName, const Caracteristique& newCarac, const Inventory& newInv, const Stuff& newStuff, const std::vector<Sort> & newSort) {
    assert(newNiv>=1);
    id=newId;
    namePerso = newName;
    nivPerso = newNiv;
    caracPerso = newCarac;
    inventairePerso = newInv;
    stuffPerso = newStuff;
    sortsPerso = newSort;
}

/** @brief Attaque (avec attaque de base) un personnage et retourne les informations relative a cette attaque
 *  @param ennemi Personnage à attaquer
 */
retourAttack Personnage::attack(Personnage& ennemi){
    retourAttack retAtt;
    if (ennemi.isAlive()) {
        retAtt.applied = true;
        int dmg = int(getStuffPerso().getWeaponDmg() + getCaracPerso().getStrength());
        dmg -= ennemi.getCaracPerso().getArmor();
        
        int dmg_dealt = ennemi.getCaracPerso().addHealth(-dmg); 
        cout << getNamePerso() << " inflige "<< dmg_dealt << " dommages a "<< ennemi.getNamePerso() << endl;
        retAtt.dmg=dmg_dealt;
        retAtt.killed = !ennemi.isAlive();
    } else {
        retAtt.applied = false;
        retAtt.killed=false;
    }
    return retAtt;
};

/** @brief Attaque (avec sort) un personnage et retourne les informations relative a cette attaque
 *  @param indexSpell Indice du sort dans le tableau à utiliser
 *  @param ennemi Personnage à attaquer
 */
retourAttack Personnage::castSpell(int indexSpell,Personnage& ennemi){
    retourAttack retAtt;
    assert(indexSpell>=0);
    if(ennemi.isAlive()) {
        if ((getCaracPerso().getMana()) >= sortsPerso[indexSpell].getManaCost()){
            retAtt.applied = true;
            float dmg = getCaracPerso().getStrength()*sortsPerso[indexSpell].getStrengthRatio();
            dmg += sortsPerso[indexSpell].getDmgFix();
            dmg -= ennemi.getCaracPerso().getArmor();
            int dmgInt = int(dmg);
            int dmg_dealt = ennemi.getCaracPerso().addHealth(-dmgInt); 
            cout << getNamePerso() << " inflige "<< dmg_dealt << " dommages a "<< ennemi.getNamePerso() << endl;
            getCaracPerso().addMana(-sortsPerso[indexSpell].getManaCost());
            retAtt.killed = !ennemi.isAlive();
            retAtt.dmg = dmg_dealt;
        }
        else{
            retAtt.applied=false;
            retAtt.dmg = 0;
            retAtt.killed= false;
        }
    } else {
        retAtt.applied=false;
        retAtt.dmg = 0;
        retAtt.killed= false;
    }
    return retAtt;
}
/** @brief Applique les effets d'un sort et le supprime de l'inventaire
 *  @param inventoryIndex Indice de l'objet dans le tableau à utiliser
 */
void Personnage::useObject(unsigned int inventoryIndex) {
    bool used = false;
    assert(inventoryIndex<inventairePerso.getNbObj());
    Objet obj=inventairePerso.getObjIndex(inventoryIndex);
    if (obj.getTypeObj() == HEALTH_POT){
        if (caracPerso.getHealth() == caracPerso.getMaxHealth()){
            cout << "already full hp" << endl;
        }
        else{
            used = true;
            caracPerso.addHealth(obj.getCarac().getHealth());
        }

    }
    else if (obj.getTypeObj() == MANA_POT){
        if (caracPerso.getMana() == caracPerso.getMaxMana()){
            cout << "already full mana" << endl;
        }
        else{
            used = true;
             caracPerso.addMana(obj.getCarac().getMana());
        }

    }
    else if (obj.getTypeObj() == STRENGTH_POT){
        used = true;
        caracPerso.addStrength(obj.getCarac().getStrength());
    }
    

    if (used){
        cout<<"Vous avez utilise: "<<obj.getObjName()<<endl;
        inventairePerso.removeObject(inventoryIndex);
    }
}

/** @brief Joue(attaque) contre un personnage
 *  @param perso Personnage cible
 */
const retourAttack Personnage::playAgainst(Personnage& perso){
    retourAttack ret = attack(perso);
    return ret;
}

/** @brief Récupère le niveau */
const int Personnage::getNivPerso() { return nivPerso; }

/** @brief Récupère le nom */
const string Personnage::getNamePerso() { return namePerso; }

/** @brief Récupère les caractéristiques */
Caracteristique& Personnage::getCaracPerso() { return caracPerso; }

/** @brief Récupère l'inventaire  */
Inventory& Personnage::getInvPerso() { return inventairePerso; }

/** @brief Récupère le stuff */
const Stuff& Personnage::getStuffPerso() { return stuffPerso; }

/** @brief Récupère un sort
 *  @param index Index du sort dans le tableau
 */
const Sort& Personnage::getSortPerso(int index)  { return sortsPerso[index]; }

/** @brief Récupère le nombre de sort  */
const unsigned int Personnage::getNbSpell(){return sortsPerso.size();}

/** @brief REtourne un booléen si le personnage est en vie */
bool Personnage::isAlive(){return (getCaracPerso().getHealth()>0);}

/** @brief Récupère l'identifiant */
const unsigned int Personnage::getId(){return id;}

/** @brief Modifie l'identifiant
 *  @param newId Nouveau identifiant
 */
void Personnage::setId(unsigned int newId) {id=newId;}