#ifndef PERSONNAGE_H
#define PERSONNAGE_H
#include <string.h>
#include "caracteristique.h"
#include "inventory.h"
#include "objet.h"
#include "stuff.h"
#include "sort.h"
/**
 * @struct retourAttack
 * @brief Structure stockant des informations nécessaires lorsqu'une attaque est effectuée 
 */
struct retourAttack{
    int dmg;
    bool applied;
    bool killed;
};

/**
 * @class Personnage
 * @brief Classe regroupant les informations d'un personnage
 */
class Personnage {
    private:
        int nivPerso;
        int id; /*!< Identifant: 0 pour le joueur principal, 10 11 12 pour les ennemis du premier niveau, 20 21 22 pour ceux du deuxieme etc */
        std::string namePerso;
        Caracteristique caracPerso;
        Inventory inventairePerso;
        Stuff stuffPerso;
    public:
        std::vector<Sort> sortsPerso;
        friend std::ostream& operator<<(std::ostream& os, Personnage& perso);
        Personnage();
        Personnage(unsigned int newId,int newNiv,const std::string newName, const Caracteristique& newCarac, 
                   const Inventory& newInv, const Stuff& newStuff, const std::vector<Sort> &newSort);
        int const getNivPerso();
        const std::string getNamePerso();
        Caracteristique& getCaracPerso();
        Inventory& getInvPerso();
        const Stuff& getStuffPerso();
        const Sort& getSortPerso(int index);
        const unsigned int getNbSpell();
        const unsigned int getId();
        void setId(unsigned int newId);
        retourAttack attack(Personnage& ennemi);
        retourAttack castSpell(int indexSpell,Personnage& ennemi);
        void useObject(unsigned int inventoryIndex);
        bool isAlive();
        const retourAttack playAgainst(Personnage& perso);
};


#endif