#include <iostream>
#include <assert.h>
#include "stuff.h"
using namespace std;

ostream& operator<<(ostream& os, const Stuff& stuff)
{
    os << "Casque: "<<stuff.getHelmetArmor() << " | Plastron: "<<stuff.getChestArmor()<<" | Degats d'arme: "<<stuff.getWeaponDmg()<<endl;
    return os;
}


Stuff::Stuff() {
    helmetArmor=0;
    chestArmor=0;
    weaponDmg=0;
}

Stuff::Stuff(int newHelmetArmor, int newChestArmor, int newWeaponDmg) {
    assert (newHelmetArmor >= 0);
    assert (newChestArmor >= 0);
    assert (newWeaponDmg >= 0);
    helmetArmor=newHelmetArmor;
    chestArmor=newChestArmor;
    weaponDmg=newWeaponDmg;
}

/** @brief Récupère l'armure du casque */
int Stuff::getHelmetArmor() const { return helmetArmor; }

/** @brief Récupère l'armure du plastron */
int Stuff::getChestArmor() const { return chestArmor; }

/** @brief Récupère les dégats de l'arme */
int Stuff::getWeaponDmg() const { return weaponDmg; }

/** @brief Modifie l'armure du casque
 *  @param newArmor Nouvelle valeur d'armure
 */
void Stuff::setHelmetArmor(int newArmor) {
    assert (newArmor >= 0);
    helmetArmor=newArmor;
}

/** @brief Modifie l'armure du plastron
 *  @param newArmor Nouvelle valeur d'armure
 */
void Stuff::setChestArmor(int newArmor) {
    assert (newArmor >= 0);
    chestArmor=newArmor;
}

/** @brief Modifie les dégats de l'arme
 *  @param newDmg Nouvelle valeur de dégats
 */
void Stuff::setWeaponDmg(int newDmg) {
    assert (newDmg >= 0);
    chestArmor=newDmg;
}