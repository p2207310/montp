#include <iostream>
#include <string.h>
#include "stuff.h"
#include "sort.h"
#include "objet.h"
#include "caracteristique.h"
#include "inventory.h"
#include "personnage.h"
#include "affichage.h"
#include "jeu.h"
#include <SDL.h>
#include <SDL_image.h>

using namespace std;
/*! \mainpage Page d'accueil - Bob the Wizard
 *
 * \section intro_sec Introduction
 *
 * Documentation des classes et des fonctions faites par doxygen
 * \subsection else Plus d'information dans le readme.txt
 *
 * 
 */

int main(){
    
    Affichage Jeu;
    Jeu.boucleJeu();
    return 0;
}
