#include <iostream>
#include <assert.h>
#include "caracteristique.h"
#include <cmath>
using namespace std;


ostream& operator<<(ostream& os, const Caracteristique& carac)
{
    os << "Force: "<<carac.getStrength()<<" | Vie :"<<carac.getHealth()<<" | Mana: "<<carac.getMana();
    os << " | Vitesse: "<<carac.getSpeed()<< " | Armure: "<<carac.getArmor();
    return os;
}

Caracteristique::Caracteristique() { strength=0; health=0; maxHealth=0; mana=0; maxMana=0; speed=0; armor=0;}

Caracteristique::Caracteristique(int newStrength, int newHealth, int newMana, int newSpeed, int newArmor) {
    assert (newStrength >=0);
    assert (newHealth >=0);
    assert (newMana >=0);
    assert (newSpeed >=0);
    assert (newArmor >=0);
    strength=newStrength;
    health=maxHealth=newHealth;
    mana=maxMana=newMana;
    speed=newSpeed;
    armor=newArmor;
}
/** @brief Récupère la force */
int Caracteristique::getStrength() const { return strength; }

/** @brief Récupère la vie */
int Caracteristique::getHealth() const { return health; }

/** @brief Récupère la vie maximum */
int Caracteristique::getMaxHealth() const { return maxHealth; }

/** @brief Récupère le mana */
int Caracteristique::getMana() const { return mana; }

/** @brief Récupère le mana maximum*/
int Caracteristique::getMaxMana() const { return maxMana; }

/** @brief Récupère la vitesse */
int Caracteristique::getSpeed() const { return speed; }

/** @brief Récupère l'armure' */
int Caracteristique::getArmor() const { return armor; }

/** @brief Modifie la force
 *  @param newStrength Nouvelle valeur de force
 */
void Caracteristique::setStrength(unsigned int newStrength) {strength=newStrength;}

/** @brief Modifie la vitesse
 *  @param newSpeed Nouvelle valeur de vitesse
 */
void Caracteristique::setSpeed(unsigned int newSpeed) {speed=newSpeed;}

/** @brief Modifie l'armure
 *  @param newArmor Nouvelle valeur d'armure
 */
void Caracteristique::setArmor(unsigned int newArmor) {armor=newArmor;}

/** @brief Ajoute/enlève de la vie
 *  @param ammount Valeur: positive pour ajouter, negative pour enlever
 */
int Caracteristique::addHealth(int ammount) {
    int healthApplied=0;
    if(health+ammount>maxHealth){
        healthApplied = maxHealth-health;
        health=maxHealth;
    }
    else if (health+ammount<0){
        healthApplied=-health;
        health=0;
    }
    else {
        health+=ammount;
        healthApplied = ammount;
    }
    return healthApplied;
}

/** @brief Ajoute/enlève du mana
 *  @param ammount Valeur: positive pour ajouter, negative pour enlever
 */
void Caracteristique::addMana(int ammount) {
    if(mana+ammount>maxMana)
        mana=maxMana;
    else if (mana+ammount<0)
        mana=0;
    else
        mana+=ammount;
}
/** @brief Ajoute/enlève de la force
 *  @param ammount Valeur: positive pour ajouter, negative pour enlever
 */
void Caracteristique::addStrength(int ammount) {
   strength+=ammount;
}