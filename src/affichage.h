#ifndef AFFICHAGE_H
#define AFFICHAGE_H
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <vector>
#include "image.h"
#include "jeu.h"

/**
 * @class Affichage
 * @brief Classe permettant l'affichage en mode graphique du jeu (SDL) 
 */
class Affichage {
    private:
        bool buttonOnScreen;
        Jeu jeu;
        SDL_Window* fenetre;
        SDL_Event evenement;
        SDL_Renderer * renderer;
        std::vector<TTF_Font *> fonts;
        std::vector<Image> fontsImage;
        
        //----------------------------
        Image cursor;
        Image background;
        Image player;
        Image ennemies[3];
        
        Image inventory;
        Image fleche;
        Image sword;
        Image spell1;
        Image spell2;
        Image healthPot;
        Image manaPot;
        Image strenghtPot;
        Image playerUp;
        Image button[2];
        Image temp;
        Image spells_animation[2][9];

        
    public:
        Affichage();
        ~Affichage();
        void renderCopyImage(const Image& im);
        int boucleJeu();
        void renderBar();
        void renderBarEnnemy();
        void renderInventory();
        int getIndexInventory(SDL_Point* mousePosition,type_obj t);
        void initEnnemies();
        void displayStats();
        void displayText(std::string text,int x,int y,int fontSize);
        void renderAll();
        void displayAttackStats(const SDL_Point& mousePosition);
        void displayEnnemyStats(const SDL_Point& mousePosition);
        void menuHandler(const SDL_Point& mousePosition,bool& quit);
        void initButtons();
        void renderSpellAnimation(int numSpell,int ennemyChosen);
};
#endif
