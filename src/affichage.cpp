#include <SDL.h>
#include <SDL_image.h>     
#include <SDL_ttf.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include "affichage.h"
#include "image.h"
#include "jeu.h"
using namespace std;

Affichage::Affichage(): jeu(){
    jeu.initJeu();
    fenetre = SDL_CreateWindow("Bob the Wizard", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1280, 720, SDL_WINDOW_SHOWN);
    renderer=SDL_CreateRenderer(fenetre, -1, 0);
    cursor.initImage(renderer,"./data/cursor.png",0,0,25,25);
    background.initImage(renderer,"./data/background.png",0,0,1280,720);
    player.initImage(renderer,"./data/player.png",0,150,250,300);
    inventory.initImage(renderer,"./data/inventaire.png",10,460,500,250);
    fleche.initImage(renderer,"./data/fleche.png",0,0,75,75);
    sword.initImage(renderer,"./data/sword.png",575,625,60,60);
    spell1.initImage(renderer,"./data/spell1.png",650,625,60,60);
    spell2.initImage(renderer,"./data/spell2.png",720,625,60,60);
    healthPot.initImage(renderer, "./data/healthPot.png",192,573,50,50);
    manaPot.initImage(renderer, "./data/manaPot.png",0,0,0,0);
    strenghtPot.initImage(renderer, "./data/StrenghtPot.png",0,0,0,0);
    playerUp.initImage(renderer, "./data/playerpowerup.png", 0,150,250,300);
    button[0].initImage(renderer, "./data/button.png",0,0,0,0);
    button[1].initImage(renderer, "./data/button.png",0,0,0,0);
    for(int i=1;i<9;i++){
        std::string path1="./data/spell1_animation/spell1_";
        std::string path2="./data/spell2_animation/spell2_";
        std::string extension = ".png";
        std::string fullPath1 = path1+to_string(i)+extension;
        std::string fullPath2 = path2+to_string(i)+extension;
        spells_animation[0][i].initImage(renderer, fullPath1.c_str(),100+i*100,250,100,100);
        spells_animation[1][i].initImage(renderer, fullPath2.c_str(),100+i*100,250,100,100);
    }
    fontsImage.push_back(temp);
    fontsImage.push_back(temp);

}

Affichage::~Affichage(){
    for (unsigned int i=0;i<fonts.size();i++)
        TTF_CloseFont(fonts[i]);
    TTF_Quit();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(fenetre);
    SDL_Quit();
}
/** @brief Applique la fonciton SDL_RenderCopy sur l'image passée en paramètre */
void Affichage::renderCopyImage(const Image& im) {
    SDL_RenderCopy(renderer, im.texture, NULL, &im.position);
}

/** @brief Initialise les images des ennemies en fonction du niveau actuel */
void Affichage::initEnnemies() {
    // ennemies.resize(ennemies.size()+1);
    // ennemies[0].initImage(renderer,"./data/corbac.png",900,180,250,300);
    //get niv
    if(jeu.getNivActuel()==0){
         for (unsigned int i=0;i<jeu.getEnemyNumber();i++) {
            ennemies[i].initImage(renderer,"./data/corbac.png",900-i*220,180,250,300);}

        }
    else{
        for (unsigned int i=0;i<jeu.getEnemyNumber();i++) {
            if(i%2==0){
                ennemies[i].initImage(renderer,"./data/corbac.png",900-i*220,180,250,300);}
            else{
                ennemies[i].initImage(renderer,"./data/gromp.png",900-i*220,200,250,300);
            }
        }
    }
}

/** @brief Affiche la barre de vie et de mana du joueur */
void Affichage::renderBar() {
    SDL_Rect vieRect ={195,500,212,15};
    int vie =jeu.getPlayer().getCaracPerso().getHealth();
    int maxVie=jeu.getPlayer().getCaracPerso().getMaxHealth();
    int mana=jeu.getPlayer().getCaracPerso().getMana();
    int maxMana=jeu.getPlayer().getCaracPerso().getMaxMana();
    int lgVie=212*vie/maxVie;

    SDL_Rect vieRectFill ={195,500,lgVie,15};
    SDL_SetRenderDrawColor(renderer,250,20,20,255);
    SDL_RenderFillRect(renderer, &vieRectFill);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderDrawRect(renderer, &vieRect);

    int lgMana=212*mana/maxMana;
    SDL_Rect manaRect ={195,525,212,15};
    SDL_Rect manaRectFill ={195,525,lgMana,15};
    SDL_SetRenderDrawColor(renderer,75,75,250,255);
    SDL_RenderFillRect(renderer, &manaRectFill);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderDrawRect(renderer, &manaRect);
    if (fonts.size()==0) {
        fonts.push_back(TTF_OpenFont("./data/deja.ttf",18));
            if (fonts[0] == nullptr){
                cout<<"Le chemin ./data/deja.ttf vers le fichier est incorrect";
                exit(1);
            }
    }
    SDL_Color col;
    col.r = 0;col.g = 0;col.b = 0;col.a = 255;
    string strVie;
    strVie=to_string(vie);
    strVie=strVie+"/";
    strVie=strVie+to_string(maxVie);
    char const* texteVie= strVie.c_str();
    fontsImage[0].setSurface(TTF_RenderText_Solid(fonts[0],texteVie,col));
    fontsImage[0].texture=SDL_CreateTextureFromSurface(renderer,fontsImage[0].surface);
    fontsImage[0].position= {290,497,50,20};
    SDL_RenderCopy(renderer, fontsImage[0].texture, NULL, &fontsImage[0].position);
    SDL_DestroyTexture(fontsImage[0].texture);
    SDL_FreeSurface(fontsImage[0].surface);

    string strMana;
    strMana=to_string(mana);
    strMana=strMana+"/";
    strMana=strMana+to_string(maxMana);
    char const* texteMana= strMana.c_str();

    fontsImage[1].setSurface(TTF_RenderText_Solid(fonts[0],texteMana,col));
    fontsImage[1].texture=SDL_CreateTextureFromSurface(renderer,fontsImage[1].surface);
    fontsImage[1].position= {290,522,50,20};
    SDL_RenderCopy(renderer, fontsImage[1].texture, NULL, &fontsImage[1].position);
    SDL_DestroyTexture(fontsImage[1].texture);
    SDL_FreeSurface(fontsImage[1].surface);
}

/** @brief Renvoie l'index de l'objet dans l'inventaire sur lequel l'utilisateur a cliqué */
int Affichage::getIndexInventory(SDL_Point* mousePosition,type_obj t){
    for (unsigned int i=0;i<jeu.getPlayer().getInvPerso().getNbObj();i++) {
        if(jeu.getPlayer().getInvPerso().getObjIndex(i).getTypeObj()==t) {
            return i;
        }
    }
    return 0;
}

/** @brief Affiche les objets de l'inventaire */
void Affichage::renderInventory(){
    Inventory inv = jeu.getPlayer().getInvPerso();
    unsigned int taille_invo = inv.getAllObj().size();
    unsigned int x_inv = 192;
    unsigned int y_inv = 573;
    healthPot.position.h=0;
    healthPot.position.w = 0;
    manaPot.position.h=0;
    manaPot.position.w = 0;
    strenghtPot.position.h=0;
    strenghtPot.position.w = 0;
    int countHealthPot = 0;
    int countManaPot = 0;
    int countStrengthPot = 0;
    
    for(unsigned int i=0;i<taille_invo;i++){
        if(inv.getObjIndex(i).getTypeObj()==HEALTH_POT){
            if(countHealthPot==0){
                healthPot.position.h=50;
                healthPot.position.w = 50;
                healthPot.position.x=x_inv;
                healthPot.position.y=y_inv;
                countHealthPot ++;
                x_inv+=54;
                if(i==3){
                    x_inv=192;
                    y_inv+=54;
                }
            }
            else{
                countHealthPot++;
            }
        }
        else if(inv.getObjIndex(i).getTypeObj()==MANA_POT){
            if(countManaPot==0) {
                manaPot.position.h=50;
                manaPot.position.w = 50;
                manaPot.position.x=x_inv;
                manaPot.position.y=y_inv;
                countManaPot++;
                x_inv+=54;
                if(i==3){
                    x_inv=192;
                    y_inv+=54;
                }
            } else {
                countManaPot++;
            }
        }
        else if(inv.getObjIndex(i).getTypeObj()==STRENGTH_POT){
            if(countStrengthPot==0){
                strenghtPot.position.w = 50;
                strenghtPot.position.h=50;
                strenghtPot.position.x=x_inv;
                strenghtPot.position.y=y_inv;
                x_inv+=54;
                countStrengthPot++;
                if(i==3){
                    x_inv=192;
                    y_inv+=54;
                }
            } else {
                countStrengthPot++;;
            }
        }
        
    }
    string nbHealthPot = to_string(countHealthPot);
    renderCopyImage(healthPot);
    renderCopyImage(manaPot);
    renderCopyImage(strenghtPot);
    if(countHealthPot>0) {
        displayText(nbHealthPot,healthPot.position.x,healthPot.position.y,10);}
    if(countManaPot>0) {
        displayText(to_string(countManaPot),manaPot.position.x,manaPot.position.y,10);}
    if(countStrengthPot>0) {
        displayText(to_string(countStrengthPot),strenghtPot.position.x,strenghtPot.position.y,10);}
}

/** @brief Affiche les statistiques du joueur */
void Affichage::displayStats(){
    int strength = jeu.getPlayer().getCaracPerso().getStrength();
    int speed = jeu.getPlayer().getCaracPerso().getSpeed();
    int armor = jeu.getPlayer().getCaracPerso().getArmor();
    displayText("Force: "+to_string(strength),40,580,10);
    displayText("Vitesse: "+to_string(speed),40,615,10);
    displayText("Armure: "+to_string(armor),40,650,10);
}

/** @brief Affiche un texte passé en paramètre
 *  @param text Texte à afficher
 *  @param x Position en x ou afficher le texte
 *  @param y Position en y ou afficher le texte
 *  @param fontSize Taille de la police d'ecriture. 
  */
void Affichage::displayText(std::string text,int x,int y,int fontSize) {
    if (fonts.size()==0) {
        fonts.push_back(TTF_OpenFont("./data/deja.ttf",18));
            if (fonts[0] == nullptr){
                cout<<"Le chemin ./data/deja.ttf vers le fichier est incorrect";
                exit(1);
            }
        
    }
    SDL_Color col;
    col.r = 255;col.g = 255;col.b = 255;col.a = 255;
    fontsImage[0].setSurface(TTF_RenderText_Solid(fonts[0],text.c_str(),col));
    fontsImage[0].texture=SDL_CreateTextureFromSurface(renderer,fontsImage[0].surface);
    int width=fontSize*text.length();
    fontsImage[0].position= {x,y,width,fontSize*2};
    SDL_RenderCopy(renderer, fontsImage[0].texture, NULL, &fontsImage[0].position);
    SDL_DestroyTexture(fontsImage[0].texture);
    SDL_FreeSurface(fontsImage[0].surface);

}

/** @brief Affiche les barres de vie des ennemis */
void Affichage::renderBarEnnemy() {
    for(unsigned int i=0;i<jeu.getEnemyNumber();i++) {
        int posX=ennemies[i].position.x+ennemies[i].position.w/2-50;
        int posY=150+ennemies[i].position.h-ennemies[i].position.y;
        SDL_Rect vieRect ={posX,posY,150,15};
        int vie =jeu.getPlayerById((jeu.getNivActuel()+1)*10+i).getCaracPerso().getHealth();
        int maxVie=jeu.getPlayerById((jeu.getNivActuel()+1)*10+i).getCaracPerso().getMaxHealth();
        int lgVie=150*vie/maxVie;

        SDL_Rect vieRectFill ={posX,posY,lgVie,15};
        SDL_SetRenderDrawColor(renderer,250,20,20,255);
        SDL_RenderFillRect(renderer, &vieRectFill);
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderDrawRect(renderer, &vieRect);
        if (fonts.size()==0) {
        fonts.push_back(TTF_OpenFont("./data/deja.ttf",18));
            if (fonts[0] == nullptr){
                cout<<"Le chemin ./data/deja.ttf vers le fichier est incorrect";
                exit(1);
            }
    }
        if (fonts[0] == nullptr){
            cout<<"Le chemin ./data/deja.ttf vers le fichier est incorrect";
            exit(1);
        }
        SDL_Color col;
        col.r = 0;col.g = 0;col.b = 0;col.a = 255;
        string strVie;
        strVie=to_string(vie);
        strVie=strVie+"/";
        strVie=strVie+to_string(maxVie);
        char const* texteVie= strVie.c_str();
        fontsImage[0].setSurface(TTF_RenderText_Solid(fonts[0],texteVie,col));
        fontsImage[0].texture=SDL_CreateTextureFromSurface(renderer,fontsImage[0].surface);
        fontsImage[0].position= {posX+10,posY-2,50,20};
        SDL_RenderCopy(renderer, fontsImage[0].texture, NULL, &fontsImage[0].position);
        SDL_DestroyTexture(fontsImage[0].texture);
        SDL_FreeSurface(fontsImage[0].surface);
    }
}
/** @brief Affiche toutes les images du niveau */
void Affichage::renderAll() {
    renderCopyImage(background);
    renderCopyImage(player);
    for(unsigned int i=0;i<jeu.getEnemyNumber();i++)
        renderCopyImage(ennemies[i]);
    renderCopyImage(inventory);
    renderCopyImage(sword);
    renderCopyImage(spell1);
    renderCopyImage(spell2);
    renderCopyImage(fleche);
    renderBar();
    renderBarEnnemy();
    renderInventory();
    displayStats();
}

/** @brief Affiche les caratéristiques des sorts */
void Affichage::displayAttackStats(const SDL_Point& mousePosition) {    
    int posXInfo = 830;
    int posYInfo = 600;
    if (SDL_PointInRect(&mousePosition, &sword.position)) {
        string strengthStr = to_string(jeu.getPlayer().getCaracPerso().getStrength());
        displayText("Degats: "+strengthStr, posXInfo,posYInfo,25);
    }
    if (SDL_PointInRect(&mousePosition, &spell1.position)) {
        string manaCostSpell1 = to_string(jeu.getPlayer().sortsPerso[0].getManaCost());
        string DmgSpell1 = to_string(int(jeu.getPlayer().sortsPerso[0].getDmgFix()+jeu.getPlayer().getCaracPerso().getStrength()*jeu.getPlayer().sortsPerso[0].getStrengthRatio()));
        displayText("Degats: "+DmgSpell1, posXInfo,posYInfo,25);
        displayText("Cout mana: "+manaCostSpell1, posXInfo,posYInfo+50,25);
    }
    if (SDL_PointInRect(&mousePosition, &spell2.position)) {
        string manaCostSpell2 = to_string(jeu.getPlayer().sortsPerso[1].getManaCost());
        string DmgSpell2 = to_string(int(jeu.getPlayer().sortsPerso[1].getDmgFix()+jeu.getPlayer().getCaracPerso().getStrength()*jeu.getPlayer().sortsPerso[1].getStrengthRatio()));
        displayText("Degats: "+DmgSpell2, posXInfo,posYInfo,25);
        displayText("Cout mana: "+manaCostSpell2, posXInfo,posYInfo+50,25);
    }
    if (SDL_PointInRect(&mousePosition, &healthPot.position))  { //hard codé pour l'instant
        displayText("Potion de soin: +20hp", posXInfo,posYInfo,15);
    }
    if (SDL_PointInRect(&mousePosition, &manaPot.position))  { //hard codé pour l'instant
        displayText("Potion de mana: +10 mana", posXInfo,posYInfo,15);
    }
    if (SDL_PointInRect(&mousePosition, &strenghtPot.position))  { //hard codé pour l'instant
        displayText("Potion de force: +10 de force | 3 tours", posXInfo,posYInfo,15);
    }
    
}

/** @brief Initialise les buttons des menus */
void Affichage::initButtons() {
    button[1].position.x = background.position.w/2-105;
    button[1].position.y = background.position.h/2+50;
    button[1].position.h = 100;
    button[1].position.w = 210;
    button[0].position.x = 0;
    button[0].position.y = 0;
    button[0].position.h = 0;
    button[0].position.w = 0;
    button[0].position.x = background.position.w/2-105;
    button[0].position.y = background.position.h/2-150;
    button[0].position.h = 100;
    button[0].position.w = 210;
}

void Affichage::menuHandler(const SDL_Point& mousePosition,bool& quit){
    if (!jeu.getPlayer().isAlive()) {
        quit=true;
        return;
    }
    if (SDL_PointInRect(&mousePosition, &button[0].position))  { //hard codé pour l'instant
        jeu.incrementNivActuel();
        jeu.initPriorite();
        initEnnemies();
        jeu.endGame = false;
        buttonOnScreen = false;
    }
    else if (SDL_PointInRect(&mousePosition, &button[1].position))  { //hard codé pour l'instant
        jeu.restart();
        jeu.assignId();
        jeu.initPriorite();
        initEnnemies();
        buttonOnScreen = false;
        jeu.endGame = false;
    }
}


/** @brief Affiche caractéristiques des ennemis */
void Affichage::displayEnnemyStats(const SDL_Point& mousePosition) {
    for (unsigned int i=0;i<jeu.getEnemyNumber();i++) {
        if (SDL_PointInRect(&mousePosition, &ennemies[i].position))  { //hard codé pour l'instant
            displayText(jeu.getPlayerById(jeu.prio[i+1]).getNamePerso(), 830,560,15);
            displayText("Force: "+to_string(jeu.getPlayerById(jeu.prio[i+1]).getCaracPerso().getStrength()), 830,590,15);
            displayText("Vitesse: "+to_string(jeu.getPlayerById(jeu.prio[i+1]).getCaracPerso().getSpeed()), 830,620,15);
            displayText("Armure: "+to_string(jeu.getPlayerById(jeu.prio[i+1]).getCaracPerso().getArmor()), 830,650,15);
            return; //Evite de supperposer deux affichage si deux hitbox s'intersectent
        }
    }
}

void Affichage::renderSpellAnimation(int numSpell,int ennemyChosen){
    int current=SDL_GetTicks();
    while(SDL_GetTicks()-current<800){
        for(int i=0;i<9;i++){
            if((current%100)>i){
                spells_animation[numSpell][i].position.x -=20*i*(ennemyChosen-1);
                spells_animation[numSpell][i].position.w -=20*(ennemyChosen-1);
                renderCopyImage(spells_animation[numSpell][i]);}
                SDL_RenderPresent(renderer);
                spells_animation[numSpell][i].position.x +=20*i*(ennemyChosen-1);
                spells_animation[numSpell][i].position.w +=20*(ennemyChosen-1);
        }
 
    }
}

/** @brief Boucle de jeu SDL principale */
int Affichage::boucleJeu(){
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Erreur d'initialisation de SDL : %s\n", SDL_GetError());
        return 1;
    }
    if (fenetre == NULL) {
        printf("Erreur de création de la fenetre : %s\n", SDL_GetError());
        return 1;
    }

    if(TTF_Init() == -1){
        fprintf(stderr, "Erreur d'initialisation de TTF_Init : %s\n", TTF_GetError());
        return 1;
    }

    bool quit = false;
    jeu.endGame=false;
    bool hasClicked;

    jeu.assignId();
    jeu.initPriorite();
    initEnnemies();

    int ennemyChosen=1;
    unsigned int currentIndexPrio=0;
    SDL_Point mousePosition;
    fleche.position.x=ennemies[0].position.x+ennemies[0].position.w/2;
    fleche.position.y=50+ennemies[0].position.h-ennemies[0].position.y;
    
    int durationStrPot = 0;
    int buff = 0;
    int attackInfo,attackInfo2,attackedEnnemy=0;
    int timeDisplayAttack,timeDisplayAttack2=-9999;
    buttonOnScreen = false;
    while (!quit) {
        hasClicked=false;

        renderAll();
        SDL_GetMouseState(&mousePosition.x, &mousePosition.y);
        displayAttackStats(mousePosition);
        displayEnnemyStats(mousePosition);
        
        while (SDL_PollEvent(&evenement)) {
            switch (evenement.type) {
                case SDL_QUIT :
                    quit = true;
                case SDL_MOUSEBUTTONDOWN:
                    
                    if (evenement.button.button == SDL_BUTTON_LEFT) {
                        hasClicked=true;
                        SDL_GetMouseState(&mousePosition.x, &mousePosition.y);
                        if(buttonOnScreen) {
                            menuHandler(mousePosition,quit);
                            durationStrPot=0;
                        }
                    }
            }
        }
        if (buttonOnScreen) {
            renderCopyImage(button[0]);
            renderCopyImage(button[1]);
            if (jeu.getNivActuel() < jeu.nbNiv()-1 && jeu.getPlayer().isAlive())
                displayText("Niveau Suivant!",button[0].position.x+10,button[0].position.y+button[0].position.h/3,13);
            else {
                if (jeu.getPlayer().isAlive())
                    displayText("Vous avez battu le jeu! ",button[0].position.x-125,button[0].position.y-150,25);
                else
                    displayText("Vous etes morts... ",button[0].position.x-125,button[0].position.y-150,25);
                displayText("Quitter!",button[0].position.x+35,button[0].position.y+button[0].position.h/3,13);
            }
            displayText("Recommencer!",button[1].position.x+10,button[1].position.y+button[0].position.h/3,15);
            SDL_RenderPresent(renderer);
            currentIndexPrio=0;
            continue;
        }

        if(!jeu.endGame) {
            if (durationStrPot>0){
                displayText(to_string(durationStrPot)+" tour(s) restants booste",0,0,10);
                renderCopyImage(playerUp);
            }

            if(jeu.prio[currentIndexPrio]==0) { //Si c'est au joueur de jouer
                if(hasClicked) {
                    retourAttack ra;
                    ra.killed = false; ra.applied=false;
                    bool endRound=false;
                    if (SDL_PointInRect(&mousePosition, &healthPot.position)) {
                        int indexInv = getIndexInventory(&mousePosition,HEALTH_POT);
                        jeu.getPlayer().useObject(indexInv);

                    } else if (SDL_PointInRect(&mousePosition, &manaPot.position)) {
                        int indexInv = getIndexInventory(&mousePosition,MANA_POT);
                        jeu.getPlayer().useObject(indexInv);

                    } else if (SDL_PointInRect(&mousePosition, &strenghtPot.position)) {
                        int indexInv = getIndexInventory(&mousePosition,STRENGTH_POT);
                        durationStrPot = jeu.getPlayer().getInvPerso().getObjIndex(indexInv).getDuration();
                        buff = jeu.getPlayer().getInvPerso().getObjIndex(indexInv).getCarac().getStrength();
                        jeu.getPlayer().useObject(indexInv);

                    }
                     else if (SDL_PointInRect(&mousePosition, &sword.position)) {
                        cout<<jeu.prio[ennemyChosen]<<endl;
                        cout << jeu.getPlayerById(jeu.prio[ennemyChosen]) << endl;
                        
                        ra=jeu.getPlayer().attack(jeu.getPlayerById(jeu.prio[ennemyChosen]));
                        if (ra.applied){
                            endRound=true;
                            timeDisplayAttack=SDL_GetTicks();
                            attackInfo=ra.dmg;
                            attackedEnnemy=ennemyChosen;
                        }
                        
                    } else if (SDL_PointInRect(&mousePosition, &spell1.position)){
                        ra=jeu.getPlayer().castSpell(0,jeu.getPlayerById(jeu.prio[ennemyChosen]));
                        if (ra.applied){ //Si le sort a pu etre lancé
                            endRound=true;
                            attackInfo=ra.dmg;
                            attackedEnnemy=ennemyChosen;
                            renderSpellAnimation(0,ennemyChosen);
                            timeDisplayAttack=SDL_GetTicks();
 
 
                        } //manquerait un else pour afficher manque de mana
 
                    } else if (SDL_PointInRect(&mousePosition, &spell2.position)){
                        ra=jeu.getPlayer().castSpell(1,jeu.getPlayerById(jeu.prio[ennemyChosen]));
                        if (ra.applied){ //Si le sort a pu etre lancé
                            endRound=true;
                            timeDisplayAttack=SDL_GetTicks();
                            attackInfo=ra.dmg;
                            attackedEnnemy=ennemyChosen;
                            renderSpellAnimation(1,ennemyChosen);
                        } //manquerait un else pour afficher manque de mana
                            
                    } else {
                        for(unsigned int i=0;i<jeu.getEnemyNumber();i++) {
                            //cout<<SDL_PointInRect(&mousePosition, &ennemies[i].position)<<endl;
                            if (SDL_PointInRect(&mousePosition, &ennemies[i].position)) {
                                ennemyChosen=i+1;
                                fleche.position.x=ennemies[i].position.x+ennemies[0].position.w/2;
                                fleche.position.y=50+ennemies[i].position.h-ennemies[0].position.y;
                            }
                        }
                    }
                    if (ra.killed){
                        jeu.getNiveaux()[jeu.getNivActuel()].nbEnnemisMorts++;
                    }
                    if(endRound){
                        if(durationStrPot>0){
                            durationStrPot -= 1;
                            if (durationStrPot==0) {
                                jeu.getPlayer().getCaracPerso().addStrength(-buff);
                            }
                        } 
                        currentIndexPrio=(currentIndexPrio+1)%jeu.prio.size();
                    }
                } 
            } else {
                retourAttack ra;
                SDL_Delay(400);
                
                if(jeu.getPlayerById(jeu.prio[currentIndexPrio]).isAlive()){
                    ra=jeu.getPlayerById(jeu.prio[currentIndexPrio]).playAgainst(jeu.getPlayer()); //pour l'instant ils ne font que auto attack 
                    timeDisplayAttack2=SDL_GetTicks();
                    attackInfo2+=ra.dmg;
                }
                currentIndexPrio=(currentIndexPrio+1)%jeu.prio.size();
            }
            if (SDL_GetTicks()-timeDisplayAttack<2500) {
                displayText("Vous avez inflige " +to_string(attackInfo)+ " degats",ennemies[attackedEnnemy-1].position.x-50,ennemies[attackedEnnemy-1].position.y,15);
            }
            if (SDL_GetTicks()-timeDisplayAttack2<2000) {
                displayText("L'ennemi vous a inflige "+to_string(attackInfo2)+" degats",250,150,10);
            } else {
                attackInfo2=0;
            }
            if(jeu.getEnemyNumber() == jeu.getNbEnnemiMort()){
                timeDisplayAttack=0;
                timeDisplayAttack2=0;
                initButtons();
                buttonOnScreen = true;
            }
                //tous les mobs du niveau sont morts
            jeu.endGame = ((!jeu.getPlayerById(0).isAlive()) || (jeu.getEnemyNumber() == jeu.getNbEnnemiMort()));
        } else {
            initButtons();
            buttonOnScreen = true;
        }
        SDL_RenderPresent(renderer);
    }
    if(jeu.getPlayerById(0).isAlive())
                cout<<"Felicitations,vous avez battu le niveau "<<1+jeu.getNivActuel()<<"!"<<endl;
            else
                cout<<"Vous etes mort au niveau "<<1+jeu.getNivActuel()<<"..."<<endl;
    SDL_Quit();
    return 0;
}