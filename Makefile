ifeq ($(OS),Windows_NT)
	INCLUDE_DIR_SDL = 	-Isrc/SDL2 \
                        -Iextern/SDL2_mingw-cb20/SDL2-2.0.12/x86_64-w64-mingw32/include/SDL2 \
						-Iextern/SDL2_mingw-cb20/SDL2_ttf-2.0.15/x86_64-w64-mingw32/include/SDL2 \
						-Iextern/SDL2_mingw-cb20/SDL2_image-2.0.5/x86_64-w64-mingw32/include/SDL2 \
						-Iextern/SDL2_mingw-cb20/SDL2_mixer-2.0.4/x86_64-w64-mingw32/include/SDL2

	LIBS_SDL = -Lextern \
			-Lextern/SDL2_mingw-cb20/SDL2-2.0.12/x86_64-w64-mingw32/lib \
			-Lextern/SDL2_mingw-cb20/SDL2_ttf-2.0.15/x86_64-w64-mingw32/lib \
			-Lextern/SDL2_mingw-cb20/SDL2_image-2.0.5/x86_64-w64-mingw32/lib \
			-Lextern/SDL2_mingw-cb20/SDL2_mixer-2.0.4/x86_64-w64-mingw32/lib \
			-lmingw32 -lSDL2main -lSDL2.dll -lSDL2_ttf.dll -lSDL2_image.dll -lSDL2_mixer.dll

else
	INCLUDE_DIR_SDL = -I/usr/include/SDL2
	LIBS_SDL = -lSDL2 -lSDL2_ttf -lSDL2_image -lSDL2_mixer -lGL
endif

all: bin/main bin/mainText

bin/mainText: obj/mainText.o obj/stuff.o obj/sort.o obj/caracteristique.o obj/objet.o obj/inventory.o obj/personnage.o obj/jeu.o
	g++ -g obj/mainText.o obj/stuff.o obj/sort.o obj/caracteristique.o obj/objet.o obj/inventory.o obj/personnage.o obj/jeu.o -o bin/mainText

obj/mainText.o: src/mainText.cpp obj/stuff.o obj/sort.o obj/caracteristique.o obj/objet.o obj/inventory.o obj/personnage.o obj/affichage.o obj/image.o
	g++ -g -Wall -c src/mainText.cpp -o obj/mainText.o

bin/main: obj/main.o obj/stuff.o obj/sort.o obj/caracteristique.o obj/objet.o obj/inventory.o obj/personnage.o obj/jeu.o obj/affichage.o obj/image.o
	g++ -g obj/main.o obj/stuff.o obj/sort.o obj/caracteristique.o obj/objet.o obj/inventory.o obj/personnage.o obj/jeu.o obj/affichage.o obj/image.o -o bin/main $(LIBS_SDL)

obj/main.o: src/main.cpp obj/stuff.o obj/sort.o obj/caracteristique.o obj/objet.o obj/inventory.o obj/personnage.o obj/affichage.o obj/image.o
	g++ -g -Wall -c src/main.cpp -o obj/main.o $(INCLUDE_DIR_SDL)

obj/jeu.o: src/jeu.cpp src/personnage.h
	g++ -g -Wall -c src/jeu.cpp -o obj/jeu.o

obj/stuff.o: src/stuff.cpp src/stuff.h
	g++ -g -Wall -c src/stuff.cpp -o obj/stuff.o

obj/sort.o: src/sort.cpp src/sort.h
	g++ -g -Wall -c src/sort.cpp -o obj/sort.o

obj/caracteristique.o: src/caracteristique.cpp src/caracteristique.h
	g++ -g -Wall -c src/caracteristique.cpp -o obj/caracteristique.o

obj/objet.o: src/objet.cpp src/objet.h src/caracteristique.h
	g++ -g -Wall -c src/objet.cpp -o obj/objet.o

obj/inventory.o: src/inventory.cpp src/inventory.h src/objet.h
	g++ -g -Wall -c src/inventory.cpp -o obj/inventory.o

obj/personnage.o: src/personnage.cpp src/inventory.h src/caracteristique.h src/objet.h src/stuff.h src/sort.h
	g++ -g -Wall -c src/personnage.cpp -o obj/personnage.o

obj/affichage.o: src/affichage.cpp src/image.h
	g++ -g -Wall -c src/affichage.cpp -o obj/affichage.o $(INCLUDE_DIR_SDL)

obj/image.o: src/image.cpp
	g++ -g -Wall -c src/image.cpp -o obj/image.o $(INCLUDE_DIR_SDL)


clean:
	rm obj/*.o bin/*
